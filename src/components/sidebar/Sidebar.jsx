import './sidebar.scss'

import {
  Dashboard,
  PersonOutlineOutlined,
  ListAltOutlined,
  Inventory2Outlined,
  LocalShippingOutlined,
  AutoGraphOutlined,
  MarkEmailUnreadOutlined,
  HealthAndSafetyOutlined,
  DescriptionOutlined,
  SettingsApplicationsOutlined,
  AccountCircleOutlined,
  LogoutOutlined,
} from '@mui/icons-material'

import { Link } from 'react-router-dom'
import { useContext } from 'react'
import { DarkModeContext } from '../../context/darkModeContext'

const Sidebar = () => {
  const { dispatch } = useContext(DarkModeContext)
  return (
    <div className='sidebar'>
      {/* -------------- top --------------- */}
      <div className='top'>
        <Link to='/'>
          <span className='logo'>lamadmin</span>
        </Link>
      </div>

      {/* ------------- center ----------------- */}
      <div className='center'>
        <ul>
          <p className='title'>MAIN</p>
          <Link to={'/'}>
            <li>
              <Dashboard className='icon' />
              <span>Dashboard</span>
            </li>
          </Link>
          <p className='title'>LIST</p>
          <Link to={'/users'}>
            <li>
              <PersonOutlineOutlined className='icon' />
              <span>Users</span>
            </li>
          </Link>
          <Link to={'/hotels'}>
            <li>
              <ListAltOutlined className='icon' />
              <span>Hotels</span>
            </li>
          </Link>
          <Link to={'/rooms'}>
            <li>
              <Inventory2Outlined className='icon' />
              <span>Rooms</span>
            </li>
          </Link>
          <Link to={'/delivery'}>
            <li>
              <LocalShippingOutlined className='icon' />
              <span>Delivery</span>
            </li>
          </Link>
          <p className='title'>INFO</p>
          <li>
            <AutoGraphOutlined className='icon' />
            <span>Stats</span>
          </li>
          <li>
            <MarkEmailUnreadOutlined className='icon' />
            <span>Notifications</span>
          </li>
          <p className='title'>SERVICE</p>
          <li>
            <HealthAndSafetyOutlined className='icon' />
            <span>System Health</span>
          </li>
          <li>
            <DescriptionOutlined className='icon' />
            <span>Logs</span>
          </li>
          <li>
            <SettingsApplicationsOutlined className='icon' />
            <span>Settings</span>
          </li>
          <p className='title'>USER</p>
          <li>
            <AccountCircleOutlined className='icon' />
            <span>Profile</span>
          </li>
          <li>
            <LogoutOutlined className='icon' />
            <span>Logout</span>
          </li>
        </ul>
      </div>

      {/* ---------------- bottom -------------- */}
      <div className='bottom'>
        <p className='title'>THEME</p>
        <div className='option'>
          <div
            className='colorOption'
            onClick={() => dispatch({ type: 'LIGHT' })}
          ></div>
          <div
            className='colorOption'
            onClick={() => dispatch({ type: 'DARK' })}
          ></div>
        </div>
      </div>
    </div>
  )
}

export default Sidebar
