import './datatable.scss'

import axios from 'axios'

import { Link, useLocation } from 'react-router-dom'

import { userColumns, userRows } from '../../datatablesource'

import { DataGrid } from '@mui/x-data-grid'
import { useState, useEffect } from 'react'

import useFetch from '../../customHook/useFetch.js'

// const columns = [
//   { field: 'id', headerName: 'ID', width: 70 },
//   { field: 'firstName', headerName: 'First name', width: 130 },
//   { field: 'lastName', headerName: 'Last name', width: 130 },
//   {
//     field: 'age',
//     headerName: 'Age',
//     type: 'number',
//     width: 90,
//   },
//   {
//     field: 'fullName',
//     headerName: 'Full name',
//     description: 'This column has a value getter and is not sortable.',
//     sortable: false,
//     width: 160,
//     // valueGetter: (params) =>
//     //   `${params.row.firstName || ''} ${params.row.lastName || ''}`,
//     renderCell: (params) => {
//       return (
//         <>
//           <span>
//             {params.row.firstName} {params.row.lastName}
//           </span>
//           <span> {params.row.age}</span>
//         </>
//       )
//     },
//   },
// ]

// const rows = [
//   { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
//   { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
//   { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
//   { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
//   { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
//   { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
//   { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
//   { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
//   { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
// ]

// This is under List.jsx which is shared by multiple page in displaying data
// we then have to get whose using it so we can dynamically throw  axios endpoints
// useLocation and split method helps us determine whose url is using

const Datatable = ({ columns }) => {
  const location = useLocation()
  const path = location.pathname.split('/')[1]

  const { loading, error, data } = useFetch(`/${path}`) // user, hotels, rooms, etc
  console.log(data)

  // create a local state to fetch data everytime there is a change
  const [list, setList] = useState(data)
  useEffect(() => {
    setList(data)
  }, [data])

  // function to delete a list dynamically depending on what endpoints
  const handleDelete = async (id) => {
    // call an API endpoint to delete
    // filter the list

    try {
      await axios.delete(`/${path}/delete/${id}`)
      setList(list.filter((item) => item._id !== id))
    } catch (error) {
      console.log(error)
    }
  }

  const actionColumn = [
    {
      field: 'action',
      headerName: 'Action',
      width: 200,
      renderCell: (params) => {
        return (
          <div className='cellAction'>
            <div className='viewButton'>
              <Link to={`/${path}/${params.row._id}`}>View</Link>
            </div>
            <div
              className='deleteButton'
              onClick={() => handleDelete(params.row._id)}
            >
              Delete
            </div>
          </div>
        )
      },
    },
  ]

  return (
    <div className='datatable'>
      <div className='datatableTitle'>
        Add new User
        <Link to={`/${path}/new`}>Add New</Link>
      </div>
      <DataGrid
        className='datagrid'
        rows={list}
        // columns={userColumns.concat(actionColumn)}
        columns={columns.concat(actionColumn)}
        pageSize={8}
        rowsPerPageOptions={[8]}
        checkboxSelection
        getRowId={(row) => row._id}
      />
    </div>
  )
}

export default Datatable
