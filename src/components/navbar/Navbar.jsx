import './navbar.scss'
import {
  SearchOutlined,
  LanguageOutlined,
  DarkModeOutlined,
  FullscreenExitOutlined,
  NotificationsNoneOutlined,
  ChatBubbleOutlineOutlined,
  ListOutlined,
} from '@mui/icons-material'
import { useContext } from 'react'
import { DarkModeContext } from '../../context/darkModeContext'

const Navbar = () => {
  const { dispatch } = useContext(DarkModeContext)
  return (
    <div className='navbar'>
      {/* ---------------- wrapper ------------------ */}
      <div className='wrapper'>
        {/* ------------------- search -------------- */}
        <div className='search'>
          <input type='text' placeholder='Search...' />
          <SearchOutlined className='icon' />
        </div>

        {/* ------------------- items --------------- */}
        <div className='items'>
          <div className='item'>
            <LanguageOutlined className='icon' />
            English
          </div>
          <div className='item'>
            <DarkModeOutlined
              className='icon'
              onClick={() => dispatch({ type: 'TOGGLE' })}
            />
          </div>
          <div className='item'>
            <FullscreenExitOutlined className='icon' />
          </div>
          <div className='item'>
            <NotificationsNoneOutlined className='icon' />
            <div className='counter'>1</div>
          </div>
          <div className='item'>
            <ChatBubbleOutlineOutlined className='icon' />
            <div className='counter'>1</div>
          </div>
          <div className='item'>
            <ListOutlined className='icon' />
          </div>
          <div className='item'>
            <img
              src='https://images.pexels.com/photos/2014422/pexels-photo-2014422.jpeg?auto=compress&cs=tinysrgb&h=130'
              alt='Profile Pic'
              className='avatar'
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Navbar
