import './single.scss'

import Sidebar from '../../components/sidebar/Sidebar'
import Navbar from '../../components/navbar/Navbar'
import Chart from '../../components/chart/Chart'
import Table from '../../components/table/Table'
import useFetch from '../../customHook/useFetch'
import { useLocation } from 'react-router-dom'

const Single = () => {
  // fetch data
  // since it is common to other entity, fetch it dynamically

  const location = useLocation()
  const path = location.pathname.slice(1)

  const { data, loading, error } = useFetch(`/${path}`)
  console.log(data)

  return (
    <div className='single'>
      <Sidebar />
      <div className='singleContainer'>
        <Navbar />

        <div className='top'>
          <div className='left'>
            {loading ? (
              'Loading... please wait..'
            ) : data ? (
              <>
                <div className='editButton'>Edit</div>
                <h1 className='title'>Information</h1>

                <div className='item'>
                  <img
                    src={data.img}
                    // alt='Item picture'
                    className='itemImg'
                  />
                  <div className='details'>
                    <div className='itemTitle'>{data.username}</div>

                    <div className='detailItem'>
                      <div className='itemKey'>Email:</div>
                      <div className='itemValue'>{data.email}</div>
                    </div>
                    <div className='detailItem'>
                      <div className='itemKey'>Phone:</div>
                      <div className='itemValue'>{data.phon}</div>
                    </div>
                    <div className='detailItem'>
                      <div className='itemKey'>City:</div>
                      <div className='itemValue'>{data.city}</div>
                    </div>
                    <div className='detailItem'>
                      <div className='itemKey'>Country:</div>
                      <div className='itemValue'>{data.country}</div>
                    </div>
                  </div>
                </div>
              </>
            ) : (
              { error }
            )}
          </div>

          <div className='right'>
            <Chart title={'User Spending (Last 6 Months)'} />
          </div>
        </div>

        <div className='bottom'>
          <div className='lista'>
            <h1 className='title'>Last Transactions</h1>
            <Table />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Single
