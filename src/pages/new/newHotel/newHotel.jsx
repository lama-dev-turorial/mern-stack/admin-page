import './newHotel.css'
import Navbar from '../../../components/navbar/Navbar'
import Sidebar from '../../../components/sidebar/Sidebar'
import { useState } from 'react'
import axios from 'axios'
import { Room } from '@mui/icons-material'
import useFetch from '../../../customHook/useFetch'

const NewHotel = ({ inputs }) => {
  const [hotel, setHotel] = useState({})
  const [files, setFiles] = useState('')
  const [rooms, setRooms] = useState([])

  // useFetch
  const { loading, error, data } = useFetch('/rooms')
  console.log(data)

  const handleChange = (e) => {
    setHotel((prev) => ({ ...prev, [e.target.id]: e.target.value }))
  }

  /**
   *
   * function to handle selected rooms
   *
   * Business Logic:
   *    1. Convert selected items to array
   *    2. Set it to local state   *
   *
   * Sample Code
   *    console.log(Array.from([1, 2, 3], x => x + x));
   *    expected output: Array [2, 4, 6]
   */
  const handleSelect = (e) => {
    console.log(e.target.selectedOptions)
    const value = Array.from(e.target.selectedOptions, (option) => option.value)
    setRooms(value)
  }

  /**
   *
   * Objective:
   *    1. Uplaod multiple files to cloudinary
   *    2. Construct the object of new hotel
   *    3. Save to MongoDB
   *
   * Business Logic:
   *    1. Convert files to FormData()
   *    2. Loop thru all files
   *    3. Append to FormData each file
   *    4. Append cloudinary preset and the upload folder
   *    5. Call axios Post method to uplaod on every loop
   *    6. Build the new hotel object
   *    7. Call the endpoint that handles Post of new hotel
   */
  const submit = async (e) => {
    e.preventDefault()

    try {
      const list = await Promise.all(
        Object.values(files).map(async (file) => {
          const data = new FormData()
          data.append('file', file)
          data.append('upload_preset', 'upload')
          const uploadRes = await axios.post(
            'https://api.cloudinary.com/v1_1/dm0fdag7y/image/upload',
            data
          )

          const { url } = uploadRes.data
          return url
        })
      )

      const newHotel = {
        ...hotel,
        rooms,
        photos: list,
        desc: 'Description',
      }
      console.log(newHotel)

      const savedHotel = await axios.post('/hotels', newHotel)
      console.log(savedHotel)
    } catch (error) {
      console.log(error)
    }
  }
  console.log(hotel)
  return (
    <div className='newHotel'>
      <Sidebar />

      <div className='nhContainer'>
        <Navbar />

        <div className='botContainer'>
          <div className='title'>
            <h1>Add New Hotel</h1>
          </div>
          {/* -------------------data area -----------------------*/}
          <div className='infoContainer'>
            <div className='photosContainer'>
              <div className='photos'>
                <img
                  src={
                    files
                      ? URL.createObjectURL(files[0])
                      : 'https://upload.wikimedia.org/wikipedia/commons/6/65/No-Image-Placeholder.svg'
                  }
                  alt='Hotel image'
                />
              </div>
              <div className='pThumbnail'>thumbnails here</div>
              <div className='file'>
                <input
                  id='file'
                  type={'file'}
                  onChange={(e) => setFiles(e.target.files)}
                  multiple
                />
              </div>
            </div>

            <div className='info'>
              {inputs.map((input) => (
                <div className='inputs' key={input._id}>
                  <label htmlFor={input.id}>{input.label}</label>
                  <input
                    id={input.id}
                    onChange={handleChange}
                    type={input.type}
                    placeholder={input.placeholder}
                  />
                </div>
              ))}

              <div className='inputs'>
                <label htmlFor=''>Featured</label>
                <div className='radioContainer'>
                  <div className='radio'>
                    <label htmlFor='yes'>Yes</label>
                    <input type='radio' name='featured' value={true} id='yes' />
                  </div>
                  <div className='radio'>
                    <label htmlFor='no'>No</label>
                    <input type='radio' name='featured' value={false} id='no' />
                  </div>
                </div>
              </div>

              <div className='infoRooms'>
                <label htmlFor='rooms'>Rooms</label>
                {loading ? (
                  'Loading... please wait..'
                ) : data ? (
                  <select
                    name='rooms'
                    id='rooms'
                    multiple
                    onChange={handleSelect}
                  >
                    {data.map((room) => (
                      <option key={room._id} value={room._id}>
                        {room.title}
                      </option>
                    ))}
                  </select>
                ) : (
                  error
                )}
              </div>

              <div className='inputs'>
                <button type='button'>Reset</button>
              </div>
              <div className='inputs'>
                <button type='button' onClick={submit}>
                  Submit
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default NewHotel
