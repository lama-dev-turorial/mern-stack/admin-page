import './new.scss'
import Navbar from '../../components/navbar/Navbar'
import Sidebar from '../../components/sidebar/Sidebar'

import DriveFolderUploadIcon from '@mui/icons-material/DriveFolderUpload'
import { useState } from 'react'
import axios from 'axios'

const New = ({ inputs, title }) => {
  const [file, setFile] = useState('')
  const [user, setUser] = useState({})

  const handleChange = (e) => {
    setUser((prev) => ({ ...prev, [e.target.id]: e.target.value }))
  }

  const submit = async (e) => {
    e.preventDefault()

    // converting to formData
    const data = new FormData()
    data.append('file', file)

    // appending cloudinary preset
    data.append('upload_preset', 'upload')

    // upload to cloudinary
    try {
      // we are separately uploading image then use the url returned to mongodb
      const uploadRes = await axios.post(
        'https://api.cloudinary.com/v1_1/dm0fdag7y/image/upload',
        data
      )
      const { url } = uploadRes.data

      // register a user
      const newUser = {
        ...user,
        img: url,
      }

      console.log('newUser', newUser)

      const savedUser = await axios.post('/auth/register', newUser)

      console.log(savedUser.data.response)
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <div className='new'>
      <Sidebar />

      <div className='newContainer'>
        <Navbar />

        <div className='top'>
          <h1>{title}</h1>
        </div>

        <div className='bottom'>
          <div className='left'>
            <img
              src={
                file
                  ? URL.createObjectURL(file)
                  : 'https://images.pexels.com/photos/1820770/pexels-photo-1820770.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500'
              }
              alt='User image'
              className='userImg'
            />
          </div>
          <div className='right'>
            <form>
              <div className='formInput'>
                <label htmlFor='file'>
                  Image: <DriveFolderUploadIcon className='icon' />
                </label>
                <input
                  id='file'
                  type={'file'}
                  style={{ display: 'none' }}
                  onChange={(e) => setFile(e.target.files[0])}
                />
              </div>
              {/* ---------------dynamic form inputs start----------------------- */}

              {inputs.map((input) => (
                <div className='formInput' key={input.id}>
                  <label>{input.label}</label>
                  <input
                    onChange={handleChange}
                    type={input.type}
                    placeholder={input?.placeholder}
                    id={input.id}
                  />
                </div>
              ))}
              {/* ---------------dynamic form inputs end----------------------- */}
              <button onClick={submit}>Send</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default New
