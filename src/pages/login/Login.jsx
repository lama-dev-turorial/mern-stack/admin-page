import './login.scss'

import axios from 'axios'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLock, faUser } from '@fortawesome/free-solid-svg-icons'

import { useContext, useState } from 'react'
import { AuthContext } from '../../context/auth/authContext'
import { useNavigate } from 'react-router-dom'

const Login = () => {
  // fields
  const navigate = useNavigate()

  // state
  const [credentials, setCredentials] = useState({
    username: undefined,
    password: undefined,
  })

  // context
  const { user, loading, error, dispatch } = useContext(AuthContext)
  console.log('user', user)
  console.log('type', typeof user)
  // function to handle login
  const login = async (e) => {
    e.preventDefault()

    dispatch({ type: 'LOGIN_START' })
    try {
      const res = await axios.post('/auth/login', credentials)
      const { details } = res.data

      if (res.data?.isAdmin) {
        dispatch({
          type: 'LOGIN_SUCCESS',
          payload: details,
        })
        console.log('navigating to home page')
        navigate('/')
      } else {
        dispatch({
          type: 'LOGIN_FAILURE',
          payload: JSON.stringify({ message: 'You are not authorized.' }),
        })
      }
    } catch (error) {
      dispatch({ type: 'LOGIN_FAILURE', payload: error.response.data.message })
    }
  }

  // function to save input upon input change
  const handleChange = (e) => {
    setCredentials((prev) => ({ ...prev, [e.target.id]: e.target.value }))
  }

  console.log(credentials)
  // console.log(user)

  return (
    <div className='login'>
      <div className='lContainer'>
        <h1>Login</h1>
        <div className='cItemContainer'>
          <div className='item'>
            <div className='title'>
              <h4>Username</h4>
            </div>
            <div className='itemInput'>
              <FontAwesomeIcon icon={faUser} />
              <input
                id='username'
                type={'text'}
                onChange={(e) => handleChange(e)}
              />
            </div>
          </div>
          <div className='item'>
            <div className='title'>
              <h4>Password</h4>
            </div>
            <div className='itemInput'>
              <FontAwesomeIcon icon={faLock} />
              <input
                type={'password'}
                id='password'
                onChange={(e) => handleChange(e)}
              />
            </div>
          </div>
          <button onClick={(e) => login(e)}>Login</button>
          {error && <div className='error'>{error}</div>}
        </div>
      </div>
    </div>
  )
}

export default Login
