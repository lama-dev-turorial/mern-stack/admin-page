export const userInputs = [
  {
    id: 'username',
    label: 'Username',
    type: 'text',
    placeholder: 'username',
  },
  {
    id: 'name',
    label: 'Name and surname',
    type: 'text',
    placeholder: 'Name and surname',
  },
  {
    id: 'email',
    label: 'Email',
    type: 'mail',
    placeholder: 'email',
  },
  {
    id: 'phone',
    label: 'Phone',
    type: 'text',
    placeholder: 'phone',
  },
  {
    id: 'password',
    label: 'Password',
    type: 'password',
  },
  {
    id: 'address',
    label: 'Address',
    type: 'text',
    placeholder: 'address',
  },
  {
    id: 'country',
    label: 'Country',
    type: 'text',
    placeholder: 'Country',
  },
]

export const hotelInputs = [
  { id: 'name', label: 'Name', type: 'text', placeholder: 'Property name' },
  { id: 'title', label: 'Title', type: 'text', placeholder: 'Property title' },
  { id: 'type', label: 'Type', type: 'text', placeholder: 'Property type' },
  {
    id: 'desc',
    label: 'Description',
    type: 'text',
    placeholder: 'Property description',
  },
  {
    id: 'cheapestPrice',
    label: 'Cheapest Price',
    type: 'text',
    placeholder: 'Property Cheapest price',
  },
  { id: 'city', label: 'City', type: 'text', placeholder: 'Property city' },
  {
    id: 'address',
    label: 'Address',
    type: 'text',
    placeholder: 'Property address',
  },
  {
    id: 'distance',
    label: 'Distance',
    type: 'text',
    placeholder: 'Property distance',
  },
]

export const productInputs = [
  {
    id: 1,
    label: 'Title',
    type: 'text',
    placeholder: 'Apple Macbook Pro',
  },
  {
    id: 2,
    label: 'Description',
    type: 'text',
    placeholder: 'Description',
  },
  {
    id: 3,
    label: 'Category',
    type: 'text',
    placeholder: 'Computers',
  },
  {
    id: 4,
    label: 'Price',
    type: 'text',
    placeholder: '100',
  },
  {
    id: 5,
    label: 'Stock',
    type: 'text',
    placeholder: 'in stock',
  },
]
