/**
 *
 * @param {*} state - state from authContext
 * @param {*} action - dispatch type from user
 * @returns - returns the appropriate payload based on action.type
 */
// Action Reducer
const AuthReducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN_START':
      return { user: null, loading: true, error: null }
    case 'LOGIN_SUCCESS':
      return { user: action.payload, loading: false, error: null }
    case 'LOGIN_FAILURE':
      return { user: null, loading: false, error: action.payload }
    case 'LOGOUT': {
      return { user: null, loading: false, error: null }
    }

    default:
      return state
  }
}

export default AuthReducer
