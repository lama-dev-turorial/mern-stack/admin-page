import { createContext, useEffect, useReducer } from 'react'
import AuthReducer from './authReducer'

// Initial State
const INITIAL_STATE = {
  user: JSON.parse(localStorage.getItem('user') || null),
  loading: false,
  error: false,
}

// Create Context
export const AuthContext = createContext(INITIAL_STATE)

// Create Provider
export const AuthContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AuthReducer, INITIAL_STATE)

  // prevent data loss upon refresh
  // will only update when user change
  useEffect(() => {
    localStorage.setItem('user', JSON.stringify(state.user))
  }, [state.user])

  // values that can be fetched
  return (
    <AuthContext.Provider
      value={{
        user: state.user,
        loading: state.loading,
        error: state.error,
        dispatch,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
