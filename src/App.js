import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'

import './style/dark.scss'
// --------- pages ------------------------------
import Home from './pages/home/Home'
import List from './pages/list/List'
import Login from './pages/login/Login'
import New from './pages/new/New'
import Single from './pages/single/Single'

// ----------inputs------------------------------
import { userInputs, productInputs, hotelInputs } from './formSource'
import { useContext } from 'react'
import { DarkModeContext } from './context/darkModeContext'
import { AuthContext } from './context/auth/authContext'

// DataGrid Columns ---------------------------------------
import { userColumns, hotelColumns, roomColumns } from './datatablesource'
import NewHotel from './pages/new/newHotel/newHotel'

function App() {
  const { darkMode } = useContext(DarkModeContext)

  // function to protect admin routes
  const ProtectedRoute = ({ children }) => {
    // look for user
    const { user } = useContext(AuthContext)
    console.log(user)

    // end and navigate to login if no user
    if (!user) {
      console.log('no admin found. navigating to login page.')
      return <Navigate to={'/login'} />
    }

    // return children if there is a user
    return children
  }
  return (
    <div className={darkMode ? 'app dark' : 'app'}>
      <BrowserRouter>
        <Routes>
          <Route path='/'>
            <Route path='login' element={<Login />} />

            <Route
              index
              element={
                <ProtectedRoute>
                  <Home />
                </ProtectedRoute>
              }
            />

            {/* ---------------- users route ----------------*/}
            <Route path='users'>
              <Route
                index
                element={
                  <ProtectedRoute>
                    <List columns={userColumns} />
                  </ProtectedRoute>
                }
              />
              <Route
                path=':userId'
                element={
                  <ProtectedRoute>
                    <Single />
                  </ProtectedRoute>
                }
              />
              <Route
                path='new'
                element={
                  <ProtectedRoute>
                    <New inputs={userInputs} title='Add New User' />
                  </ProtectedRoute>
                }
              />
            </Route>
            {/* ---------- end of users route ---------------*/}

            {/* ---------------- hotel routes ---------------- */}
            <Route path='hotels'>
              <Route
                index
                element={
                  <ProtectedRoute>
                    <List columns={hotelColumns} />
                  </ProtectedRoute>
                }
              />
              <Route
                path=':productId'
                element={
                  <ProtectedRoute>
                    <Single />
                  </ProtectedRoute>
                }
              />
              <Route path='new' element={<NewHotel inputs={hotelInputs} />} />
            </Route>
            {/* ------------ end of product routes ------------- */}

            {/* ---------------- order routes ---------------- */}
            <Route path='rooms'>
              <Route
                index
                element={
                  <ProtectedRoute>
                    <List columns={roomColumns} />
                  </ProtectedRoute>
                }
              />
              <Route
                path=':orderId'
                element={
                  <ProtectedRoute>
                    <Single />
                  </ProtectedRoute>
                }
              />
            </Route>
            {/* ------------ end of order routes ------------- */}

            {/* ---------------- delivery routes ---------------- */}
            <Route path='delivery'>
              <Route
                index
                element={
                  <ProtectedRoute>
                    <List />
                  </ProtectedRoute>
                }
              />
              <Route
                path=':deliveryId'
                element={
                  <ProtectedRoute>
                    <Single />
                  </ProtectedRoute>
                }
              />
            </Route>
            {/* ------------ end delivery routes ------------- */}
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
